﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlComida.Library.Helpers;
using ControlComida.Library.Model;

namespace ControlComida.Web.Controllers
{
    public class SolicitudComidaController : Controller
    {
        //
        // GET: /SolicitudComida/

        private IEnumerable<mHorarioTrabajo> _HorarioTrabajo;
        private IEnumerable<mComedor> _Comedor;
        private IEnumerable<mAreaSolicitante> _AreaSolicitud;
        private IEnumerable<mTipoComida> _TipoComida;

        private void dropDownList()
        {
            _Comedor = ComedorHelper.GetAllComedores();

            ViewData["Comedor"] = _Comedor.Select(r => new SelectListItem
            {
                Text = r.DescripcionComedor,
                Value = r.IdComedor.ToString()
            }).ToList();

            _AreaSolicitud = AreaSolicitanteHelper.GetAllArea();

            ViewData["AreaSolicitud"] = _AreaSolicitud.Select(r => new SelectListItem
            {
                Text = r.DesccripcionArea,
                Value = r.IdArea.ToString()
            }).ToList();

            _TipoComida = TipoComidaHelper.GetAllTiposComida();

            ViewData["tipoComida"] = _TipoComida.Select(r => new SelectListItem
            {
                Text = r.DescTipoComida,
                Value = r.IdTipoComida.ToString()
            }).ToList();

        }

        public ActionResult Index()
        {
            var vSolicitud = VistaSolicitudHelper.GetAllVistaSolicitudes();
            return View(vSolicitud);
        }

        [HttpPost]
        public ActionResult Index(List<mSolicitudEnc> vSolicitud)
        {
            return View(vSolicitud);
        }

        [HttpGet]
        public ActionResult Create()
        {
            dropDownList();
            return View();
        }

        [HttpPost]
        public ActionResult Create(mSolicitudEnc vSolicitudEnc)
        {
            dropDownList();
            vSolicitudEnc.FechaIngreso = DateTime.Now;
            vSolicitudEnc.FechaSolicitud = DateTime.Now;
            vSolicitudEnc.UsuarioAgrega = Environment.UserName;
            vSolicitudEnc.Estatus = "Solicitado";

            TempData["Numero_Solicitud"] =  SolicitudEncHelper.SetAllSolicitudesEnc(vSolicitudEnc);
            TempData["IdTipoComida"] = vSolicitudEnc.IdTipoComida;
            TempData["IdArea"] = vSolicitudEnc.IdArea;

            return RedirectToAction("Create", "Solicitud");
        }

        public PartialViewResult GetRow(mSolicitudDet SolcitudNueva)
        {
            return PartialView("pCreate", SolcitudNueva);
        }

    }
}
