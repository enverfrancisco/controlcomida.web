﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlComida.Library.Helpers;
using ControlComida.Library.Model;

namespace ControlComida.Web.Controllers
{
    public class TipoComidaController : Controller
    {
        //
        // GET: /AreaSolicitante/

        public ActionResult Index()
        {
            var TiposComida= TipoComidaHelper.GetAllTiposComida();
            return View(TiposComida);
        }

        [HttpPost]
        public ActionResult Index(List<mTipoComida> tiposComida)
        {
            return View(tiposComida);
        }


        //
        // GET: /AreaSolicitante/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /AreaSolicitante/Create

        public ActionResult Create(mTipoComida vTiposComida)
        {
            TipoComidaHelper.SetAllTiposComida(vTiposComida);
            return View();
        }

        //
        // POST: /AreaSolicitante/Create

        //[HttpPost]
        //public ActionResult Create(mAreaSolicitante vArea)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here
        //        AreaSolicitanteHelper.SetAllAreas(vArea);
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /AreaSolicitante/Edit/5

        public ActionResult Edit(int id)
        {

            return View();
        }

        //
        // POST: /AreaSolicitante/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /AreaSolicitante/Delete/5

        public ActionResult Delete(int id)
        {

            TipoComidaHelper.deleteTipoComida(id);

            return View();
        }

        //
        // POST: /AreaSolicitante/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
