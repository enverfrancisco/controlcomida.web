﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlComida.Library.Helpers;
using ControlComida.Library.Model;

namespace ControlComida.Web.Controllers
{
    public class SolicitudController : Controller
    {
        
        private IEnumerable<mHorarioTrabajo> _HorarioTrabajo;

        private void dropDownList()
        {

            _HorarioTrabajo = HorarioTrabajoHelper.GetAllHorariosTrabajo();

            ViewData["Horarios"] = _HorarioTrabajo.Select(r => new SelectListItem
            {
                Text = r.DescripcionHorTrabajo,
                Value = r.CodigoHorTrabajo.ToString()
            }).ToList();
        }

        //
        // GET: /Solicitud/

        public ActionResult Index()
        {
            var vSolicitud = VistaSolicitudHelper.GetAllVistaSolicitudes();
            return View(vSolicitud);          
        }


        [HttpGet]
        public ActionResult Create()
        {
            dropDownList();
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(mSolicitudDet vSolicitud)
        {
            dropDownList();

            vSolicitud.NumeroSolicitud = (int)@TempData["Numero_Solicitud"];
            vSolicitud.TipoComida = (int)@TempData["IdTipoComida"];
            vSolicitud.AreadeSolicitud = (int)@TempData["IdArea"];
            
            SolicitudDetHelper.SetAllSolicitudesDet(vSolicitud);

            if (vSolicitud.IDecision == true)
            {
                @TempData["Numero_Solicitud"] = vSolicitud.NumeroSolicitud;
                @TempData["IdTipoComida"] = vSolicitud.TipoComida;
                @TempData["IdArea"] = vSolicitud.AreadeSolicitud;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "SolicitudComida");
            }
        }

        public JsonResult SearchEmpleado(int? vCodigoEmpleado)
        {
            string NombreEmpleado = EmpleadosHelper.SearchEmpleado(vCodigoEmpleado);
            return Json(NombreEmpleado);
        }

        public ActionResult Aprobar()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {

            return View();
        }

        //
        // POST: /AreaSolicitante/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
